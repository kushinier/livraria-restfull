# livraria

Desenvolvimento de uma livraria para amostra do meu trabalho feito em cima das frameworks angular, spring boot com postgresql

# Instrução de uso

- Para iniciar o projeto apenas entre na pasta livraria e execute o LivrariaApplication.java e acesse a url: http://localhost:8080 ou 
gerar o projeto em jar/war e fazer a execução via tomcat.

- Para gerar uma nova aplicação, entre na pasta livraria-client e execute o comando "npm install", logo em seguida execute o comando "ng build" para
gerar o front-end lado servidor.

# Dependencias

- JDK 11
- NODEJS
- ANGULAR
- SBRING BOOT