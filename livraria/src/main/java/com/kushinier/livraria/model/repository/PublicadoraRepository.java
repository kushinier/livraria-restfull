package com.kushinier.livraria.model.repository;

import com.kushinier.livraria.model.entity.Publicadora;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PublicadoraRepository extends CrudRepository<Publicadora, Integer> {
    
}
