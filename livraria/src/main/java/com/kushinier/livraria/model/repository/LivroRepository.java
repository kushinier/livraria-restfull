package com.kushinier.livraria.model.repository;

import com.kushinier.livraria.model.entity.Livro;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LivroRepository extends CrudRepository<Livro, Integer> {

}
