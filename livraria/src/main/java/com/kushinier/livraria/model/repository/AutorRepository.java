package com.kushinier.livraria.model.repository;

import com.kushinier.livraria.model.entity.Autor;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AutorRepository extends CrudRepository<Autor, Integer> {
    
}
