package com.kushinier.livraria.service;

public interface CommonService<E> {
    
    public abstract void salvar(E entity);

    public abstract void deletar(Integer id) throws Exception;

    public abstract void atualizar(E entity, Integer id) throws Exception;

    public abstract Iterable<E> pegarTodos();

    public abstract E pegaUm(Integer id) throws Exception;
}
