package com.kushinier.livraria.service;

import java.util.Optional;

import com.kushinier.livraria.model.entity.Autor;
import com.kushinier.livraria.model.repository.AutorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AutorService implements CommonService<Autor> {

    @Autowired
    private AutorRepository repository;

    private static Autor autor;

    public AutorService() {
        autor = new Autor();
    }

    @Override
    public void salvar(Autor entity) {
        repository.save(entity);        
    }

    @Override
    public void deletar(Integer id) throws Exception {
        Optional<Autor> o = repository.findById(id);
        if(o.isPresent()) {
            o.map(a -> {
                repository.delete(a);
                return Void.TYPE;
            });
        } else {
            throw new Exception("Autor não encontrado!");
        }
    }

    @Override
    public void atualizar(Autor entity, Integer id) throws Exception {
        Optional<Autor> o = repository.findById(id);
        if(o.isPresent()) {
            o.map(a -> {
                entity.setId(a.getId());
                repository.save(entity);
                return Void.TYPE;
            });
        } else {
            throw new Exception("Autor não encontrado");
        }
    }

    @Override
    public Iterable<Autor> pegarTodos() {
        return repository.findAll();
    }

    @Override
    public Autor pegaUm(Integer id) throws Exception {
        Optional<Autor> o = repository.findById(id);
        if(o.isPresent()) {
            o.map(a -> {
                autor.setId(a.getId());
                autor.setNome(a.getNome());
                autor.setLivros(a.getLivros());
                return Void.TYPE;
            });
        } else {
            throw new Exception("Autor não encontrado!");
        }
        return autor;
    }
}
