package com.kushinier.livraria.service;

import java.util.Optional;

import com.kushinier.livraria.model.entity.Usuario;
import com.kushinier.livraria.model.repository.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements CommonService<Usuario>{

    @Autowired
    private UsuarioRepository repository;

    private static Usuario usuario;

    public UsuarioService() {
        usuario = new Usuario();
    }

    @Override
    public void salvar(Usuario entity) {
        repository.save(entity);        
    }

    @Override
    public void deletar(Integer id) throws Exception {
        Optional<Usuario> o = repository.findById(id);
        if(o.isPresent()) {
            o.map(usuario -> {
                repository.delete(usuario);
                return Void.TYPE;
            });
        } else {
            throw new Exception("Usuario não encontrado!");
        }
    }

    @Override
    public void atualizar(Usuario entity, Integer id) throws Exception {
        Optional<Usuario> o = repository.findById(id);
        if(o.isPresent()) {
            o.map(usuario -> {
                entity.setId(usuario.getId());
                repository.save(entity);
                return Void.TYPE;
            });
        } else {
            throw new Exception("Usuario não encontrado");
        }
    }

    @Override
    public Iterable<Usuario> pegarTodos() {
        return repository.findAll();
    }

    @Override
    public Usuario pegaUm(Integer id) throws Exception {
        Optional<Usuario> o = repository.findById(id);
        if(o.isPresent()) {
            o.map(u -> {
                usuario.setId(u.getId());
                usuario.setNome(u.getNome());
                usuario.setUsuario(u.getUsuario());
                usuario.setEmail(u.getEmail());
                return Void.TYPE;
            });
        } else {
            throw new Exception("Usuario não encontrado!");
        }
        return usuario;
    }    
}
