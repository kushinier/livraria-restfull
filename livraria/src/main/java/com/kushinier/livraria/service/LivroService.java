package com.kushinier.livraria.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.kushinier.livraria.model.entity.Autor;
import com.kushinier.livraria.model.entity.Livro;
import com.kushinier.livraria.model.repository.AutorRepository;
import com.kushinier.livraria.model.repository.LivroRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LivroService implements CommonService<Livro>{

    @Autowired
    private LivroRepository repository;

    @Autowired
    private AutorRepository autorRepository;

    private static Livro livro;

    public LivroService() {
        livro = new Livro();
    }

    @Override
    public void salvar(Livro entity) {
        // Para salvar via post se necessario.    
    }
    
    @Override
    public void deletar(Integer livro_id) throws Exception {
        Optional<Livro> o = repository.findById(livro_id);
        if(o.isPresent()) {
            o.map(livro -> {
                repository.delete(livro);
                return Void.TYPE;
            });            
        } else {
            throw new Exception("Livro não encontrado ou Autor não encontrado!");
        }
    }

    @Override
    public void atualizar(Livro entity, Integer id) throws Exception {
        Optional<Autor> o = autorRepository.findById(id);
        o.ifPresent(a -> {
            List<Autor> autores = new ArrayList<Autor>();
            autores.add(a);
            entity.setAutors(autores);
            repository.save(entity);
        });
        o.orElseThrow(() -> {
           return new Exception("Autor não encontrado");
        });
    }

    @Override
    public Iterable<Livro> pegarTodos() {
        return repository.findAll();
    }

    @Override
    public Livro pegaUm(Integer id) throws Exception {
        Optional<Livro> o = repository.findById(id);
        if(o.isPresent()) {
            o.map(l -> {
                livro.setId(l.getId());
                livro.setNome(l.getNome());
                livro.setDataEntrada(l.getDataEntrada());
                livro.setAutors(l.getAutors());
                livro.setUsuario(l.getUsuario());
                livro.setPublicadora(l.getPublicadora());
                return Void.TYPE;
            });
        } else {
            throw new Exception("Livro não encontrado!");
        }
        return livro;
    }
}
