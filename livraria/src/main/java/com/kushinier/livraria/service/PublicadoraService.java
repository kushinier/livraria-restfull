package com.kushinier.livraria.service;

import java.util.Optional;

import com.kushinier.livraria.model.entity.Publicadora;
import com.kushinier.livraria.model.repository.PublicadoraRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PublicadoraService implements CommonService<Publicadora> {

    @Autowired
    private PublicadoraRepository repository;

    private static Publicadora publicadora;

    public PublicadoraService() {
        publicadora = new Publicadora();
    }

    @Override
    public void salvar(Publicadora entity) {
        repository.save(entity);        
    }

    @Override
    public void deletar(Integer id) throws Exception {
        Optional<Publicadora> o = repository.findById(id);
        if(o.isPresent()) {
            o.map(p -> {
                repository.delete(p);
                return Void.TYPE;
            });
        } else {
            throw new Exception("Publicadora não encontrado!");
        }
    }

    @Override
    public void atualizar(Publicadora entity, Integer id) throws Exception {
        Optional<Publicadora> o = repository.findById(id);
        if(o.isPresent()) {
            o.map(p -> {
                entity.setId(p.getId());
                repository.save(entity);
                return Void.TYPE;
            });
        } else {
            throw new Exception("Publicadora não encontrado");
        }
    }

    @Override
    public Iterable<Publicadora> pegarTodos() {
        return repository.findAll();
    }

    @Override
    public Publicadora pegaUm(Integer id) throws Exception {
        Optional<Publicadora> o = repository.findById(id);
        if(o.isPresent()) {
            o.map(p -> {
                publicadora.setId(p.getId());
                publicadora.setNome(p.getNome());
                publicadora.setLivros(p.getLivros());
                return Void.TYPE;
            });
        } else {
            throw new Exception("Publicadora não encontrado!");
        }
        return publicadora;
    }  
}
