import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutorComponent } from './autor/autor.component';
import { HomeComponent } from './home/home.component';
import { LivrosComponent } from './livros/livros.component';
import { PublicadoraComponent } from './publicadora/publicadora.component';
import { UsuarioComponent } from './usuario/usuario.component';

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "livro", component: LivrosComponent},
  {path: "publicadora", component: PublicadoraComponent},
  {path: "usuario", component: UsuarioComponent},
  {path: "autor", component: AutorComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
