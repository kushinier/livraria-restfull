import { Autor } from "../autor/Autor";
import { Publicadora } from "../publicadora/Publicadora";
import { Usuario } from "../usuario/Usuario";

export class Livro {
    id: number;
    nome: string;
    dataEntrada: string;
    usuario: Usuario;
    publicadora: Publicadora;
    autors: Autor[];
}