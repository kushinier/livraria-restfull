import { Component, OnInit } from '@angular/core';
import { Autor } from '../autor/Autor';
import { AutorService } from '../autor/autor.service';
import { Publicadora } from '../publicadora/Publicadora';
import { PublicadoraService } from '../publicadora/publicadora.service';
import { Livro } from './Livro';
import { LivroService } from './livro.service';
import { LivroDto } from './LivroDto';

@Component({
  selector: 'app-livros',
  templateUrl: './livros.component.html',
  styleUrls: ['./livros.component.css']
})
export class LivrosComponent implements OnInit {

  livros: Livro[];
  livro: Livro;
  livroDto: LivroDto;
  publicadoras: Publicadora[];
  autores: Autor[];
  id: number;
  sucesso: boolean = false;
  erro: boolean = false;
  botao: boolean = false;
  message: string;

  constructor(
    private service: LivroService,
    private publicadoraService: PublicadoraService,
    private autorService: AutorService
  ) {
    this.livro = new Livro();
  }

  ngOnInit(): void {
    this.service.listaLivros()
      .subscribe(response => {
        this.livros = response;
      });
    this.autorService.listaAutores()
      .subscribe(response => {
        this.autores = response;
      })
    this.publicadoraService.listaPublicadoras()
      .subscribe(response => {
        this.publicadoras = response;
      })
  }

  onSubmit() {
    if (this.livro.nome == "" || this.livro.nome == null) {
      this.message = "Campo nome obrigatório!";
      this.sucesso = false;
      this.erro = true;
    } else {
      this.livroDto = {
        nome: this.livro.nome,
        publicadora: {
          id: this.livro.publicadora.id
        }
      }
      this.service.cadastrarLivro(this.livroDto, this.id)
        .subscribe(response => {
          this.message = "Salvo com sucesso";
          this.erro = false;
          this.sucesso = true;
          this.botao = false;
          this.livro = new Livro();
          this.id = -1;
          this.ngOnInit();
        });
    }
  }

  deletarLivro(livro_id: number) {
    this.service.deletarLivro(livro_id)
      .subscribe(response => {
        this.erro = false;
        this.sucesso = true;
        this.message = "Deletado com sucesso!"
        this.ngOnInit();
      }, error => {
        this.sucesso = false;
        this.erro = true;
        this.message = "Erro: livro contem um Autor vinculado a ele! Solução: favor primeiro excluir o vinculo com o autor!"
      })
  }

  atualizarLivro(id: number) {
    this.service.pegarLivro(id)
      .subscribe(response => {
        this.botao = true;
        this.livro = response;
        this.sucesso = false;
      })
  }

  cancelarAtualizacao() {
    this.livro = new Livro();
    this.botao = false;
  }
}
