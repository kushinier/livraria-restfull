import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from "../../../environments/environment";
import { Livro } from './Livro';
import { LivroDto } from './LivroDto';

@Injectable({
  providedIn: 'root'
})
export class LivroService {

  url: string = environment.apiUrl + "/livro";

  constructor(
    private http: HttpClient
  ) { }

  pegarLivro(id: number) : Observable<Livro> {
    return this.http.get<Livro>(`${this.url}/${id}`);
  }

  listaLivros() : Observable<Livro[]> {
    return this.http.get<Livro[]>(this.url);
  }

  cadastrarLivro(Livro: LivroDto, id: number) : Observable<Livro> {
    return this.http.put<Livro>(`${this.url}/${id}`, Livro);
  }

  deletarLivro(livro_id: number) : Observable<any> {
    return this.http.delete<any>(`${this.url}/${livro_id}`);
  }
}
