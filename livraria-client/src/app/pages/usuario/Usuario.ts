export class Usuario {
    id: number;
    usuario: string;
    nome: string;
    email: string;
    password: string;
}