import { Component, OnInit } from '@angular/core';
import { Usuario } from './Usuario';
import { UsuarioService } from './usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  usuarios: Usuario[];
  usuario: Usuario;
  sucesso: boolean = false;
  erro: boolean = false;
  botao: boolean = false;
  message: string;

  constructor(
    private service: UsuarioService
  ) {
    this.usuario = new Usuario();  
  }

  ngOnInit(): void {
    this.service.listaUsuarios()
    .subscribe(response => {
      this.usuarios = response;
    })
  }

  onSubmit() {
    if(this.usuario.nome == "" || this.usuario.nome == null) {
      this.message = "Campo nome obrigatório!";
      this.sucesso = false;
      this.erro = true;
    } else {
    this.service.cadastrarUsuario(this.usuario)
    .subscribe(response => {
      this.message = "Salvo com sucesso";
      this.erro = false;
      this.sucesso = true;
      this.botao = false;
      this.usuario = new Usuario();
      this.ngOnInit();
    });
    }
  }

  deletarUsuario(id: number) {
    this.service.deletarUsuario(id)
    .subscribe(response => {
      this.erro = false;
      this.sucesso = true;
      this.message = "Deletado com sucesso!"
      this.ngOnInit();
    }, error => {
      this.sucesso = false;
      this.erro = true;
      this.message = "Erro: Usuario contem um livro locado em seu nome! Solução: favor primeiro devolver/excluir o livro cadastrado!"
    })
  }

  atualizarUsuario(id: number) {
      this.service.pegarUsuario(id)
      .subscribe(response => {
        this.botao = true;
        this.usuario = response;
        this.sucesso = false;
      })
  }

  cancelarAtualizacao() {
    this.usuario = new Usuario();
    this.botao = false;
  }
}
