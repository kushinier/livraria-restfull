import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from "../../../environments/environment";
import { Usuario } from './Usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  
  url: string = environment.apiUrl + "/usuario";

  constructor(
    private http: HttpClient
  ) { }

  pegarUsuario(id: number) : Observable<Usuario> {
    return this.http.get<Usuario>(`${this.url}/${id}`);
  }

  listaUsuarios() : Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.url);
  }

  cadastrarUsuario(Usuario: Usuario) : Observable<Usuario> {
    return this.http.post<Usuario>(this.url, Usuario);
  }

  deletarUsuario(id: number) : Observable<any> {
    return this.http.delete<any>(`${this.url}/${id}`);
  }
}
