import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from "../../../environments/environment";
import { Publicadora } from './Publicadora';

@Injectable({
  providedIn: 'root'
})
export class PublicadoraService {

  url: string = environment.apiUrl + "/publicadora";

  constructor(
    private http: HttpClient
  ) { }

  pegarPublicaora(id: number) : Observable<Publicadora> {
    return this.http.get<Publicadora>(`${this.url}/${id}`);
  }

  listaPublicadoras() : Observable<Publicadora[]> {
    return this.http.get<Publicadora[]>(this.url);
  }

  cadastrarPublicadora(publicadora: Publicadora) : Observable<Publicadora> {
    return this.http.post<Publicadora>(this.url, publicadora);
  }

  deletarPublicadora(id: number) : Observable<any> {
    return this.http.delete<any>(`${this.url}/${id}`);
  }
}
