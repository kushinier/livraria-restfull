import { Component, OnInit } from '@angular/core';
import { Publicadora } from './Publicadora';
import { PublicadoraService } from './publicadora.service';

@Component({
  selector: 'app-publicadora',
  templateUrl: './publicadora.component.html',
  styleUrls: ['./publicadora.component.css']
})
export class PublicadoraComponent implements OnInit {

  publicadora: Publicadora;
  publicadoras: Publicadora[];
  sucesso: boolean = false;
  erro: boolean = false;
  botao: boolean = false;
  message: string;

  constructor(
    private service: PublicadoraService
  ) {
    this.publicadora = new Publicadora();
  }

  setPublicadoras(publicadoras: Publicadora[]) {
    this.publicadoras = publicadoras;
  }

  ngOnInit(): void {
    this.service.listaPublicadoras()
    .subscribe(response => {
      this.publicadoras = response;
    })
  }

  onSubmit() {
    if(this.publicadora.nome == "" || this.publicadora.nome == null) {
      this.message = "Campo nome obrigatório!";
      this.sucesso = false;
      this.erro = true;
    } else {
    this.service.cadastrarPublicadora(this.publicadora)
    .subscribe(response => {
      this.message = "Salvo com sucesso";
      this.erro = false;
      this.sucesso = true;
      this.botao = false;
      this.publicadora = new Publicadora();
      this.ngOnInit();
    });
    }
  }

  deletarPublicadora(id: number) {
    this.service.deletarPublicadora(id)
    .subscribe(response => {
      this.erro = false;
      this.sucesso = true;
      this.message = "Deletado com sucesso!"
      this.ngOnInit();
    }, error => {
      this.sucesso = false;
      this.erro = true;
      this.message = "Erro: Publicadora contem um livro cadastrado! Solução: favor primeiro excluir o livro cadastrado!"
    })
  }

  atualizarPublicadora(id: number) {
      this.service.pegarPublicaora(id)
      .subscribe(response => {
        this.botao = true;
        this.publicadora = response;
        this.sucesso = false;
      })
  }

  cancelarAtualizacao() {
    this.publicadora = new Publicadora();
    this.botao = false;
  }
}
