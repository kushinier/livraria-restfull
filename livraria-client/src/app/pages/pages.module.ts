import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PagesRoutingModule } from './pages-routing.module';
import { HomeComponent } from './home/home.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { PublicadoraComponent } from './publicadora/publicadora.component';
import { AutorComponent } from './autor/autor.component';
import { LivrosComponent } from './livros/livros.component';
import { PagesComponent } from './pages.component';
import { HeaderComponent } from '../header/header.component';
import { NavbarComponent } from '../header/navbar/navbar.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { FooterComponent } from '../footer/footer.component';

@NgModule({
  declarations: [
    HeaderComponent,
    NavbarComponent,
    SidebarComponent,
    HomeComponent,
    FooterComponent,
    UsuarioComponent,
    PublicadoraComponent,
    AutorComponent,
    LivrosComponent,
    PagesComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    FormsModule
  ],
  exports: [
    HeaderComponent,
    NavbarComponent,
    HomeComponent,
    FooterComponent,
    UsuarioComponent,
    PublicadoraComponent,
    AutorComponent,
    LivrosComponent,
    PagesComponent
  ]
})
export class PagesModule { }
