import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from "../../../environments/environment";
import { Autor } from './Autor';

@Injectable({
  providedIn: 'root'
})
export class AutorService {

  url: string = environment.apiUrl + "/autor";

  constructor(
    private http: HttpClient
  ) { }

  pegarAutor(id: number) : Observable<Autor> {
    return this.http.get<Autor>(`${this.url}/${id}`);
  }

  listaAutores() : Observable<Autor[]> {
    return this.http.get<Autor[]>(this.url);
  }

  cadastrarAutor(Autor: Autor) : Observable<Autor> {
    return this.http.post<Autor>(this.url, Autor);
  }

  deletarAutor(id: number) : Observable<any> {
    return this.http.delete<any>(`${this.url}/${id}`);
  }
}
