import { Component, OnInit } from '@angular/core';
import { Autor } from './Autor';
import { AutorService } from './autor.service';

@Component({
  selector: 'app-autor',
  templateUrl: './autor.component.html',
  styleUrls: ['./autor.component.css']
})
export class AutorComponent implements OnInit {

  autores: Autor[];
  autor: Autor;
  sucesso: boolean = false;
  erro: boolean = false;
  botao: boolean = false;
  message: string;

  constructor(
    private service: AutorService
  ) {
    this.autor = new Autor();
  }

  ngOnInit(): void {
    this.service.listaAutores()
      .subscribe(response => {
        this.autores = response;
      })
  }

  onSubmit() {
    if (this.autor.nome == "" || this.autor.nome == null) {
      this.message = "Campo nome obrigatório!";
      this.sucesso = false;
      this.erro = true;
    } else {
      this.service.cadastrarAutor(this.autor)
        .subscribe(response => {
          this.message = "Salvo com sucesso";
          this.erro = false;
          this.sucesso = true;
          this.botao = false;
          this.autor = new Autor();
          this.ngOnInit();
        });
    }
  }

  deletarAutor(id: number) {
    this.service.deletarAutor(id)
      .subscribe(response => {
        this.erro = false;
        this.sucesso = true;
        this.message = "Deletado com sucesso!"
        this.ngOnInit();
      }, error => {
        this.sucesso = false;
        this.erro = true;
        this.message = "Erro: Autor contem um livro cadastrado! Solução: favor primeiro excluir o livro cadastrado!"
      })
  }

  atualizarAutor(id: number) {
    this.service.pegarAutor(id)
      .subscribe(response => {
        this.botao = true;
        this.autor = response;
        this.sucesso = false;
      })
  }

  cancelarAtualizacao() {
    this.autor = new Autor();
    this.botao = false;
  }
}
